import { random } from "faker";
import { expect } from "chai";
import { get, post } from "request-promise-native"
import { dbQuery, syncDB } from "./utils/db";

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;

describe('Tracker Server', function () {

  this.timeout(30000);
  before(function (done) {
    setTimeout(done, 15000);
  })

  const dataMove = {
    "rider_id": 4,
    "north": random.number({ min: 1, max: 20 }),
    "south": random.number({ min: 1, max: 20 }),
    "west": random.number({ min: 1, max: 20 }),
    "east": random.number({ min: 1, max: 20 }),
    "createdAt": new Date(),
    "updatedAt": new Date()
  }

  beforeEach(async function () {
    await syncDB();

    // reset database
    await dbQuery({ type: "remove", table: "track_events" });

    // feed data
    this.tracks = (await dbQuery({
      type: "insert",
      table: "track_events",
      values: dataMove,
      returning: ["rider_id", "north",
        "south", "west", "east", "createdAt"
      ]
    }))[0][0];
  });

  describe('Movement', function () {
    it('should return logs movement of a rider', async function () {
      const response = await get(
        `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`,
        { json: true }
      );
      expect(response.ok).to.be.true;
    });

    it('should insert new record movement of a rider', async function () {
      const response = await post(`http://${TRACKER_HOST}:${TRACKER_PORT}/track`, {
        body: {
          rider_id: '4',
          north: '10',
          west: '0',
          east: '0',
          south: '0'
        },
        json: true
      });
      expect(response.ok).to.be.true;
    });
  });
});
